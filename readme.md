# Palestra do FISL 15

Ela aconteceu no dia 07 de maio de 2014 às 16:00

Segue abaixo o vídeo para assistir a gravação:

(hemingway.softwarelivre.org/fisl15/high/41d/sala41d-high-201405071602.ogv)

# Licença

Attribution-NonCommercial 4.0 International 

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>
