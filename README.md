# Apresentação básica sobre o Docker

Essa apresentação foi pensada para ser executada com o software [pinpoint](https://wiki.gnome.org/action/show/Apps/Pinpoint), mas depois mudei de ideia e fiz em HTML5, com uma palestra antiga, que usei do meu amigo Aurélio Hercket.

Ela foi inicialmente criada para minha palestra no evento [Linguágil 2015](http://linguagil.com.br/).

Apresentei ela no [FLISOL Salvador 2015](http://softwarelivre.org/flisol-ssa/programacao-flisol-salvador-2015)

# Licença

Attribution-NonCommercial 4.0 International 

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>
